Our visualization can be found at http://cs171.tommymacwilliam.com. Due to the terms of the Facebook API, this code
cannot be run from any other domain.

index.php: login screen for the applicaton
viz.php: main visualization page, contains all ProcessingJS code
wall.php: requests wall posts from the Facebook Graph API
newsfeed.php: requests newsfeed posts from the Facebook Graph API
processing-1.1.0.min.js: minified version of the ProcessingJS library (v. 1.1)
img/: background texture and loading gif
css:/ stylesheets
css/styles.css: stylesheet for index.php and viz.php
css/Aristo: Aristo jQuery UI theme
